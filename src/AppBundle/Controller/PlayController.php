<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class PlayController extends Controller
{
    /**
     * @Route("/game/new")
     */
    public function newAction(Request $request)
    {
        $board = $this->proxyCall($request);
        return $this->render('AppBundle:Game:board.html.twig', ['board' => $board]);
    }

    /**
     * @Route("/api/doc")
     */
    public function apiDocAction(Request $request)
    {
        return $this->proxyCall($request);
    }

    /**
     * @Route("/game/{direction}/play")
     */
    public function playAction(Request $request)
    {
        return $this->proxyCall($request);
    }

    /**
     * @Route("/game/boards/{board}")
     */
    public function getBoardAction(Request $request)
    {
        return $this->proxyCall($request);
    }

    /**
     * @Route("/game/boards")
     */
    public function getBoardsAction(Request $request)
    {
        return $this->render('AppBundle:Game:boards.html.twig', ['boards' => $this->proxyCall($request)]);
    }

    private function proxyCall(Request $request, $uri = null)
    {
        if (!$uri) {
            $uri =  $request->getRequestUri();
        }
        $client = $this->get('csa_guzzle.client.esb');

        return json_decode($client->request($request->getMethod(), $uri . '.json', $request->request->all())->getBody()->getContents(), true);
    }
}
