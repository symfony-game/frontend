<?php

namespace Tests\AppBundle;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

trait TruncateDatabaseOnSetupTrait
{
    protected function setUp()
    {
        if (!$this instanceof KernelTestCase) {
            throw new \RuntimeException('Your class should be an instance of %s', KernelTestCase::class);
        }
        parent::setUp();
        static::bootKernel();
        static::$kernel
            ->getContainer()
            ->get('doctrine.dbal.default_connection')
            ->executeQuery('TRUNCATE TABLE board');
    }
}
