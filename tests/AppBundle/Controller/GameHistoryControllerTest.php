<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tests\AppBundle\TruncateDatabaseOnSetupTrait;

class GameHistoryControllerTest extends WebTestCase
{
    use TruncateDatabaseOnSetupTrait;

    public function test_list()
    {
        $client = static::createClient();
        $client->request('GET', '/game/new.json');
        $client->request('GET', '/game/new.json');
        $client->request('GET', '/game/new.json');
        $client->request('GET', '/game/boards.json');

        $list = json_decode($client->getResponse()->getContent(), true);

        $this->assertCount(3, $list);
    }
}
