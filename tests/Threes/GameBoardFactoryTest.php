<?php

namespace Tests\Threes;

use PHPUnit\Framework\TestCase;
use Threes\GameBoardFactory;

class GameBoardFactoryTest extends TestCase
{

    public function test_random()
    {
        $board = (new GameBoardFactory())->create();

        $oneCount = 0;
        $twoCount = 0;
        $threeCount = 0;

        foreach ($board->getGrid() as $line) {
            foreach ($line as $value) {
                if (1 === $value) {
                    $oneCount++;
                    continue;
                }
                if (2 === $value) {
                    $twoCount++;
                    continue;
                }
                if (3 === $value) {
                    $threeCount++;
                    continue;
                }
            }
        }

        $this->assertGreaterThan(0, $oneCount);
        $this->assertGreaterThan(0, $twoCount);
        $this->assertGreaterThan(0, $threeCount);
        $this->assertEquals(9, $oneCount + $twoCount + $threeCount);
    }
}
