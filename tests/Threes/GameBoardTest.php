<?php

namespace Tests\Threes;

use Doctrine\Bundle\DoctrineBundle\Tests\TestCase;
use Threes\BoardInterface;
use Threes\Board;

class GameBoardTest extends TestCase
{

    public function test_moves_the_board()
    {
        $board = $this->getMockBuilder(BoardInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $board->expects($this->at(0))->method('move')->with('up');
        $board->expects($this->at(1))->method('move')->with('down');
        $board->expects($this->at(2))->method('move')->with('left');
        $board->expects($this->at(3))->method('move')->with('right');

        $board->move('up');
        $board->move('down');
        $board->move('left');
        $board->move('right');
    }

    public function test_cant_move()
    {
        $board = new Board();


        $this->assertFalse($board->canMove('up'));
        $this->assertFalse($board->canMove('down'));
        $this->assertFalse($board->canMove('left'));
        $this->assertFalse($board->canMove('right'));
    }

    public function test_can_move()
    {
        $board = new Board();


        $board->setGrid([
            [0, 0, 0, 0],
            [0, 1, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
        ]);

        $this->assertTrue($board->canMove('up'));
        $this->assertTrue($board->canMove('down'));
        $this->assertTrue($board->canMove('left'));
        $this->assertTrue($board->canMove('right'));
    }

    public function test_is_not_over()
    {
        $board = new Board();

        $board->setGrid([
            [0, 0, 0, 0],
            [0, 1, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
        ]);

        $this->assertTrue($board->isNotOver());
    }

    public function test_is_over()
    {
        $board = new Board();

        $board->setGrid([
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
        ]);

        $this->assertTrue($board->isOver());
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Invalid move "wrong", valid moves are [up, down, left, right]
     */
    public function test_wrong_move()
    {
        $board = new Board();
        $board->move('wrong');
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function test_wrong_callable_move()
    {
        $board = new Board();
        $board->move('getGrid');
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function test_wrong_can_move()
    {
        $board = new Board();
        $board->canMove('wrong');
    }
}
