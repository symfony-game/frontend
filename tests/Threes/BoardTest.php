<?php

namespace Tests\Threes;

use PHPUnit\Framework\TestCase;
use Threes\Board;

class BoardTest extends TestCase
{

    public function test_new()
    {
        $board = new Board();

        $this->assertEquals(
            [
                [0, 0, 0, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ],
            $board->getGrid()
        );
    }

    public function test_up()
    {
        $board = new Board();

        $board->setGrid([
            [0, 1, 1, 0],
            [1, 0, 1, 1],
            [0, 0, 1, 0],
            [0, 0, 1, 1],
        ]);

        $board->move('up');

        $this->assertEquals(
            [
                [1, 1, 1, 1],
                [0, 0, 1, 0],
                [0, 0, 1, 1],
                [0, 0, 1, 0],
            ],
            $board->getGrid()
        );
    }

    public function test_up_add()
    {
        $board = new Board();

        $board->setGrid([
            [1, 2, 3, 3],
            [2, 1, 3, 3],
            [0, 0, 0, 1],
            [0, 0, 0, 2],
        ]);

        $board->move('up');

        $this->assertEquals(
            [
                [3, 3, 6, 6],
                [0, 0, 0, 1],
                [0, 0, 0, 2],
                [0, 0, 0, 0],
            ],
            $board->getGrid()
        );
    }

    public function test_down()
    {
        $board = new Board();

        $board->setGrid([
            [0, 0, 1, 1],
            [1, 0, 1, 0],
            [0, 0, 1, 1],
            [0, 1, 1, 0],
        ]);

        $board->move('down');

        $this->assertEquals(
            [
                [0, 0, 1, 0],
                [0, 0, 1, 1],
                [1, 0, 1, 0],
                [0, 1, 1, 1],
            ],
            $board->getGrid()
        );
    }

    public function test_down_add()
    {
        $board = new Board();

        $board->setGrid([
            [0, 0, 0, 1],
            [0, 0, 0, 2],
            [2, 2, 3, 3],
            [1, 1, 3, 3],
        ]);

        $board->move('down');

        $this->assertEquals(
            [
                [0, 0, 0, 0],
                [0, 0, 0, 1],
                [0, 0, 0, 2],
                [3, 3, 6, 6],
            ],
            $board->getGrid()
        );
    }

    public function test_right()
    {
        $board = new Board();

        $board->setGrid([
            [1, 0, 0, 0],
            [0, 0, 0, 1],
            [1, 1, 1, 1],
            [1, 0, 1, 0],
        ]);

        $board->move('right');

        $this->assertEquals(
            [
                [0, 1, 0, 0],
                [0, 0, 0, 1],
                [1, 1, 1, 1],
                [0, 1, 0, 1],
            ],
            $board->getGrid()
        );
    }

    public function test_right_add()
    {
        $board = new Board();

        $board->setGrid([
            [0, 0, 2, 1],
            [0, 0, 1, 2],
            [0, 0, 3, 3],
            [1, 2, 3, 3],
        ]);

        $board->move('right');

        $this->assertEquals(
            [
                [0, 0, 0, 3],
                [0, 0, 0, 3],
                [0, 0, 0, 6],
                [0, 1, 2, 6],
            ],
            $board->getGrid()
        );
    }

    public function test_left()
    {
        $board = new Board();

        $board->setGrid([
            [0, 1, 0, 0],
            [1, 0, 0, 0],
            [1, 1, 1, 1],
            [0, 1, 0, 1],
        ]);

        $board->move('left');

        $this->assertEquals(
            [
                [1, 0, 0, 0],
                [1, 0, 0, 0],
                [1, 1, 1, 1],
                [1, 0, 1, 0],
            ],
            $board->getGrid()
        );
    }

    public function test_left_add()
    {
        $board = new Board();

        $board->setGrid([
            [1, 2, 0, 0],
            [2, 1, 0, 0],
            [3, 3, 0, 0],
            [3, 3, 1, 2],
        ]);

        $board->move('left');

        $this->assertEquals(
            [
                [3, 0, 0, 0],
                [3, 0, 0, 0],
                [6, 0, 0, 0],
                [6, 1, 2, 0],
            ],
            $board->getGrid()
        );
    }
}
